import { FETCH_USR } from '../type/AuthType';
const initialState = {
    token: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USR:
            return {
                ...state,
                token: action.payload
            };
        default:
            return state;
    }
};