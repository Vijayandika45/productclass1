import { FETCH_USR } from '../type/AuthType';

export const auth = data => {
    console.log(data)
    return {
        type: FETCH_USR,
        payload: data
    };
};