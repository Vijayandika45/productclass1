import React, { Component } from 'react'
import { Text, View, Image, Button, Alert, ImageBackground } from 'react-native'
import { Card } from 'native-base'
import { connect } from 'react-redux'
import Axios from 'axios'

class Account extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data : {
                image: '../img/anya.jpg'
            }
        }
    }
    detail = async () => {
        const data = async () => await Axios.get(
            'https://asuransi-glints-academy.herokuapp.com/api/user/show',
            {
                headers: {
                    Authorization: this.props.auth.token
                }
            }
        )

        data()
            .then(res => {
                console.log(res.data.result)
                this.setState({
                    data: res.data.result
                })
            })
            .catch(err => {
                console.log(err)
            })
    }

    componentDidMount() {
        this.detail()
    }

    render() {
        return (
            <View>
                 <ImageBackground style={{ width: '100%', height: '100%'}}>
                <View style={{ marginTop: 20, marginHorizontal: 30, paddingLeft: 100 }}>
                    <Image source={{uri : this.state.data.image}} style={{ marginBottom: 20, width: 100, height: 100, borderRadius: 180, alignItems: 'center', marginLeft: 20 }} />
                    <Text style={{ fontSize: 24, marginLeft: 10, color: '#A2474E', fontWeight: 'bold' ,alignItems: 'center'}}>{this.state.data.username}</Text>
                    <Text style={{ fontSize: 15, marginLeft: -10, color: '#ACA6A6' }}>Yogyakarta, Indonesia</Text>
                </View>

                <View style={{ marginTop: 30, marginHorizontal: 10 }}>
                    <Card title="Local Modules">
                        <View>
                            <Text style={{ marginTop: 20, marginHorizontal: 20, fontSize: 20, color: '#EB8094' }}>Kesehatan Jiwa</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Status </Text>
                                <Text style={{ color: 'green' }}>Aktif </Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Nomor Polis </Text>
                                <Text>0908989899900 </Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Nomor Jaminan </Text>
                                <Text>0228817 </Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Premi </Text>
                                <Text>Rp500.000,- </Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Pembayaran Selanjutnya </Text>
                                <Text>30 Agustus 2018</Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text>Batas Waktu Proteksi </Text>
                                <Text>30 Maret 2025</Text>
                            </View>
                        </View>

                        <View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10, marginBottom: 30 }}>
                                <Text>Lihat Polis </Text>
                                <Button
                                    title="Download Polis"
                                    onPress={() => Alert.alert('Not Found Data')}
                                />
                            </View>
                        </View>
                    </Card>
                </View>
                </ImageBackground>
            </View>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default (connect(mapStateToProps)(Account));