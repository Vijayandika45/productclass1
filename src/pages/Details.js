import React, { Component } from 'react'
import { Text, View, ImageBackground, Dimensions, Image } from 'react-native'
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import { Container, Header, Content, Card, CardItem, Thumbnail, Button, Icon, Left, Body, Input, Item } from 'native-base';
import { connect } from 'react-redux'



class Details extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            title: '',
            body: '',
            date: ''
        }
    }
    componentDidMount() {
        this.getKonten()
    }

    getKonten = async () => {
        const { navigation } = this.props;
        const id = await navigation.getParam('id', '');
        console.log(id)
        try {
            const getData = async () => await Axios.get(`https://asuransi-glints-academy.herokuapp.com/api/insurance/detail/${id}`, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            getData()
                .then(res => {
                    this.setState({
                        title: res.data.content.title,
                        body: res.data.content.body
                    })
                    console.log(res)
                })
                .catch(err => {
                    console.log(err)
                })
        }
        catch (e) {
            console.log(err)
        }
    }

    //     <View>
    //     <Text>
    //         ID: {JSON.stringify(this.props.navigation.getParam('itemId', 'NO-ID'))}
    //     </Text>
    //     <Image
    //         source={{ uri: this.props.navigation.getParam('image', 'null') }}
    //         style={{
    //             width: 100,
    //             height: 100
    //         }}
    //     />
    // </View>

    render() {
        return (
            <Container>
                <Header
                    style={{
                        display: 'flex',
                        backgroundColor: '#F85F73'
                    }}
                >
                    <Left
                        style={{
                            flex: 1
                        }}
                    >
                        <Icon
                            type="MaterialIcons"
                            name="arrow-back"
                            style={{
                                color: 'black'
                            }}
                            onPress={
                                () => {
                                    this.props.navigation.pop()
                                }
                            }
                        />
                    </Left>

                </Header>
                <Content>
                    <Card style={{ flex: 0 }}>
                        <CardItem>
                            <Left>
                                <Thumbnail source={require('./../icon/promo.png')} />
                                <Body>
                                    <Text>Name  :{JSON.stringify(this.props.navigation.getParam('itemId', 'NO-ID'))}</Text>
                                    <Text>Price   : {JSON.stringify(this.props.navigation.getParam('Price'))}</Text>
                                </Body>
                            </Left>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Image source={{ uri: this.props.navigation.getParam('image', 'null') }} style={{ height: 200, width: 350, flex: 1 }} />
                                <Text style={{ marginTop: 20 }}>Desc : {JSON.stringify(this.props.navigation.getParam('Desc'))}</Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Button transparent textStyle={{ color: '#87838B' }}>
                                    <Icon name="logo-github" />
                                    <Item>
                                        <Input placeholder="Add Comment..." />
                                    </Item>
                                </Button>
                            </Left>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default (connect(mapStateToProps)(Details));
