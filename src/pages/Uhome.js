import React, { Component } from 'react'
import { Text, View, Image, TextInput, ScrollView, ToastAndroid, ImageBackground } from 'react-native'
import { Container, Header, Item, Input, Button, Card, CardItem, Thumbnail, Icon, Left, Body, Right, Content, Spinner } from 'native-base';
import { connect } from 'react-redux'
import Axios from "axios";

class Uhome extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            datas: [],
            loading: true
        }
    }
    componentDidMount() {
        this.getdata()
    }
    getdata = async () => {
        try {
            const data = async () => Axios.get(
                'https://asuransi-glints-academy.herokuapp.com/api/insurance/'
            )

            data()
                .then(res => {
                    this.setState({
                        data: res.data.result
                    })
                    console.log(this.state.data)

                    this.getdataPromo()
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (error) {
            console.log(error)
        }
    }

    getdataPromo = async () => {
        try {
            const data = async () => Axios.get(
                'https://asuransi-glints-academy.herokuapp.com/api/promo'
            )

            data()
                .then(res => {
                    this.setState({
                        datas: res.data.result,
                        loading: false
                    })
                    console.log(this.state.data)
                })
                .catch(err => {
                    console.log(err)
                })
        } catch (error) {
            console.log(error)
        }
    }

    render() {
        const loops = this.state.data.map(value => {
            return (
                <View style={{ marginRight: 16 }}>
                    <CardItem key={value._id} button cardBody
                        onPress={
                            () => this.props.navigation.navigate('Details', {
                                itemId: value.title,
                                image: value.image,
                                Price: value.price,
                                Desc: value.description
                            })
                        } style={{ width: 150, height: 150, borderRadius: 20 }}
                    >
                        <Image source={{ uri: value.image }} style={{ width: 150, height: 150, borderRadius: 20, flex: 1 }} />
                    </CardItem>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 12 }}> {`${value.title.substring(0, 20)}`}</Text>
                </View >
            )
        })
        const looping = this.state.datas.map(valueP => {
            return (

                <View style={{ marginRight: 16 }}>
                    <CardItem key={valueP._id} button cardBody
                        onPress={
                            () => this.props.navigation.navigate('Details', {
                                itemId: valueP.title,
                                image: valueP.image,
                                Price: valueP.price,
                                Desc: valueP.description
                            })
                        } style={{ width: 150, height: 150, borderRadius: 20 }}
                    >
                        <Image source={{ uri: valueP.image }} style={{ width: 150, height: 150, borderRadius: 20, flex: 1 }} />
                    </CardItem>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 12 }}>{`${valueP.title.substring(0, 20)}`}</Text>
                </View>
            )
        })
        if (this.state.loading) {
            return (
                <Container>
                    <Header transparent />
                    <View>
                        <Spinner />
                    </View>
                </Container>
            )
        } else {
            return (
                <View style={{ flex: 1}}>

                    <Container>
                        <ImageBackground style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>

                            <View>
                                <ScrollView>
                                    <View>
                                        {/* search bar */}
                                        <View style={{ marginHorizontal: 17, flexDirection: 'row', paddingTop: 15 }}>
                                            <View style={{ position: 'relative', flex: 1, marginTop: 10 }}>
                                                <TextInput placeholder="Do you want asurance?" style={{ borderWidth: 1, borderColor: '#E8E8E8', borderRadius: 25, height: 40, fontSize: 13, paddingLeft: 45, paddingRight: 20, backgroundColor: 'white', marginRight: 18, marginBottom: 20 }} />
                                                <Image source={require('./../icon/search.png')} style={{ position: 'absolute', top: 5, left: 12 }} />
                                            </View>

                                            <View style={{ width: 35, alignItems: 'center', justifyContent: 'center' }}>
                                                <Image source={require('./../icon/promo.png')} />
                                            </View>
                                        </View>

                                        {/* DATA INSURANCE ALL  */}
                                        <CardItem button cardBody
                                            onPress={
                                                () => ToastAndroid.show('Hehe', ToastAndroid.SHORT)
                                            }
                                        >
                                            <Image source={require('./../img/promo1.jpg')} style={{ height: 200, width: null, flex: 1 }} />
                                        </CardItem>


                                        <View style={{ marginTop: 16, marginHorizontal: 16, borderBottomWidth: 1, marginBottom: 20 }}>

                                        </View>
                                    </View>

                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 16, paddingHorizontal: 16 }}>
                                        <Text>Promo</Text>
                                        <Text style={{ color: 'green' }}>See All</Text>
                                    </View>

                                    {/* Corousel Promo */}
                                    <ScrollView horizontal style={{ flexDirection: 'row', paddingLeft: 16 }}>
                                        {looping}

                                        <View style={{ borderBottomColor: 'blue', marginTop: 16, marginHorizontal: 16, borderBottomWidth: 1, marginBottom: 20 }}></View>
                                    </ScrollView>

                                    <View style={{ marginTop: 16, marginHorizontal: 16, borderBottomWidth: 1, marginBottom: 30 }}></View>


                                    {/* Corousel Asuransi */}
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 16, paddingHorizontal: 16 }}>
                                        <Text>Asuransi</Text>
                                        <Text style={{ color: 'green' }}>See All</Text>
                                    </View>

                                    <ScrollView horizontal style={{ flexDirection: 'row', paddingLeft: 16 }}>
                                        {loops}
                                    </ScrollView>

                                </ScrollView>

                            </View>
                        </ImageBackground>
                    </Container>
                </View >
            )
        }
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default (connect(mapStateToProps)(Uhome));