//import packga.json
import React, { Component } from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Text } from 'native-base'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction'

//Import pages 
import UhomeScreen from './Uhome';
import NotificationScreeen from './Notification'
import HelpScreen from './Help'
import  AccountScreen from './Account';
import { createBottomTabNavigator } from 'react-navigation-tabs';

//Tombol Back(FadeScreen.js)
const HomeStack = createStackNavigator(
  {
    //Defination of Navigaton from setting screen
    Home: { screen: UhomeScreen },
    
  },
  {
    headerMode: 'none'
  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#ffffff',
      },
      headerTintColor: '#FBE8D3',

      title: 'Home',
        
      //Header title
    },
  }
);

const NotificationStack = createStackNavigator(
  {
    //Defination of Navigaton from setting screen
    Notification: { screen: NotificationScreeen },

  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#F85F73',
      },
      headerTintColor: '#FFFFFF',
      title: 'Notifikasi',
      //Header title
    },
  }
);

const HelpStack = createStackNavigator(
  {
    //Defination of Navigaton from setting screen
    Help: { screen: HelpScreen },

  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#F85F73',
      },
      headerTintColor: '#FFFFFF',
      title: 'Help',
      //Header title
    },
  }
);

const AccountStack = createStackNavigator(
  {
    //Defination of Navigaton from setting screen
    Account: { screen: AccountScreen },

  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#F85F73',
      },
      headerTintColor: '#FFFFFF',
      title: 'Account',
      //Header title
    },
  }
);




//Kegunaan Fungsi
const home = createBottomTabNavigator({
  Home: { screen: HomeStack },
  Notification : {screen : NotificationStack},
  Help : {screen : HelpStack},
  Account : {screen : AccountStack}
},
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        } else if (routeName === 'Dictionary') {
          iconName = `ios-checkmark-circle${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'Notification') {
          iconName = `ios-notifications${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'Help') {
          iconName = `ios-help-circle${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'Account') {
          iconName = `ios-person${focused ? '' : '-outline'}`;
        }
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),

  }

);



//Output Fungsi
const mapStateToProps = state => ({
  auth: state.auth
});

export default createAppContainer(connect(mapStateToProps)(home));


