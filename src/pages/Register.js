import React, { Component } from 'react'
import { Text, View, ImageBackground,ToastAndroid } from 'react-native'
import { Card, Button, Item, Container, CardItem, Body, Input, Content, Right } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { withNavigation } from 'react-navigation'
import Modal from "react-native-modal";
import { connect } from 'react-redux';
import { auth } from '../redux/action/AuthAction'
import Axios from 'axios'

const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: id => dispatch(auth(id))
    }
}

class Register extends Component {

    constructor(props){
        super(props)
        this.state={
            name:'',
            email:'',
            username:'',
            password:'',
            isModalVisible: false
        }
    }
    
    register = async => {
        try {
            console.log('Kepencet')
            const regis = async (objparam) => await Axios.post(
                'https://asuransi-glints-academy.herokuapp.com/api/user/client',
                objparam
            )

            regis({
                'name' : this.state.name,
                'username' : this.state.username,
                'email' : this.state.email,
                'password' : this.state.password,

            })
            .then(res => {
                console.log(res)
                this.setState({
                    name: null,
                    email: null,
                    username: null,
                    password: null
                })
                ToastAndroid.show('Berhasil Daftar!', ToastAndroid.SHORT)
                this.props.navigation.navigate('Login')
            })
            .catch(e => {
                console.log(e)
                this.setState({
                    name: null,
                    email: null,
                    username: null,
                    password: null
                })
                ToastAndroid.show('Daftar Gagal!', ToastAndroid.SHORT)
                this.props.navigation.navigate('Register')
            })
        }
        catch(err){
            console.log(err)
        }
    }


    
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    render() {
        return (

            <KeyboardAwareScrollView>
                <Container>
                    <ImageBackground source={require('../img/ganteng2.png')} style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                        <Card transparent style={{ width: '80%', height: '90%', justifyContent: 'center', backgroundColor: 'rgba(255,255,255,0)', borderColor: 'rgba(255,255,255,0)' }}>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 20, color: '#f85f73', alignSelf: 'center', fontWeight: 'bold' }}>Register</Text>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', height: '12%' }}>
                                <Body>
                                    <Item regular style={{

                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input placeholder="Name" 
                                        onChangeText={(text) => this.setState({name : text})}
                                        style={{ color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', height: '12%' }}>
                                <Body>
                                    <Item regular style={{
                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input placeholder="Username" 
                                        onChangeText={(text) => this.setState({username : text})}
                                        style={{ color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', height: '12%' }}>
                                <Body>
                                    <Item regular style={{
                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input placeholder="Email" 
                                        onChangeText={(text) => this.setState({email : text})}
                                        style={{ color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', height: '12%' }}>
                                <Body>
                                    <Item regular style={{

                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input secureTextEntry={true} placeholder="Password" 
                                        onChangeText={(text) => this.setState({password : text})}
                                        style={{ backgroundColor: 'rgba(255,255,255,0.6)', color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', height: '12%', justifyContent: 'center' }}>
                                <Button regular 
                                onPress={()=> this.register()}
                                style={{ backgroundColor: '#f85f73', width: '50%', justifyContent: 'center' }}>
                                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#fbe8d3' }}>Sign Up</Text>
                                </Button>
                            </CardItem>
                            <CardItem style={{ borderColor: 'rgba(255,255,255,0)', backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                <Body>
                                    <Text style={{ alignSelf: 'center', color: 'white', }}>By Signing up , you agree to our</Text>
                                    <Button transparent onPress={this.toggleModal} style={{ justifyContent: 'center' }}>
                                        <Text style={{ alignSelf: 'center', color: '#f85f73', fontWeight: 'bold', fontSize: 13 }}>Terms & Privacy Policy</Text>
                                    </Button>
                                    <Text style={{ alignSelf: 'center', color: 'white' }}>Have An Account ?</Text>
                                    <Button transparent onPress={() => this.props.navigation.navigate('Login')} style={{ justifyContent: 'center' }}>
                                        <Text style={{ alignSelf: 'center', color: '#f85f73', fontWeight: 'bold', fontSize: 18 }}>Sign In</Text>
                                    </Button>
                                </Body>
                            </CardItem>
                        </Card>

                        <Modal isVisible={this.state.isModalVisible}>
                            <Card style={{ flex: 1, backgroundColor: '#FBE8D3' }}>
                                <CardItem style={{ justifyContent: 'center', backgroundColor: '#F85F73', borderColor: 'rgba(255,255,255,0)' }}>
                                    <Text style={{
                                        color: '#FBE8D3',
                                        fontSize: 18
                                    }}>Terms & Privacy Policy</Text>
                                    <Right>
                                        <Button title="Hide modal" onPress={this.toggleModal} transparent >

                                            <Text style={{
                                                color: '#FBE8D3',
                                                fontSize: 25
                                            }}>X</Text>
                                        </Button>
                                    </Right>
                                </CardItem>
                                <CardItem style={{
                                    height: '85%',
                                    backgroundColor: '#FBE8D3'
                                }}>
                                    <Body>
                                        {/* https://www.cermati.com/artikel/apa-itu-hukum-asuransi-dan-bagaimana-cara-kerjanya */}
                                        <Text style={{
                                            color: '#283c63',
                                            fontSize: 15
                                        }}>1. Perjanjian asuransi wajib memenuhi Pasal 1320 KUH Perdata, di mana perjanjian tersebut bersifat adhesif, yang artinya isi perjanjian tersebut telah ditentukan oleh perusahaan asuransi melalui kontrak standard.</Text>
                                        <Text style={{
                                            color: '#283c63',
                                            fontSize: 15
                                        }}>2. Di dalam asuransi terdapat dua pihak yang terlibat pada perjanjian tersebut, yakni pihak penanggung dan pihak tertanggung, yang mana kedua pihak ini berbeda.</Text>
                                        <Text style={{
                                            color: '#283c63',
                                            fontSize: 15
                                        }}>3. Asuransi memiliki sejumlah premi yang merupakan bukti bahwa tertanggung setuju untuk melakukan perjanjian asuransi.</Text>
                                        <Text style={{
                                            color: '#283c63',
                                            fontSize: 15
                                        }}>4. Perjanjian asuransi membuat pihak tertanggung dan pihak penanggung terikat untuk melaksanakan kewajibannya masing-masing.</Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </Modal>

                    </ImageBackground>
                </Container>
            </KeyboardAwareScrollView>

        )
    }
}
export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Register));