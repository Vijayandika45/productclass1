import React, { Component } from 'react'
import { TextInput, View, Image, Text, ScrollView, ToastAndroid, ImageBackground } from 'react-native'
import { connect } from 'react-redux'

class Help extends Component {
    render() {
        return (
            <View style={{ backgroundColor: 'pink', flex: 1 }}>
                <ImageBackground source={require('../img/ganteng2.png')} style={{ width: '100%', height: '100%'}}>
                <ScrollView>
                    <View style={{ backgroundColor: 'white' }}>
                        <View style={{ backgroundColor: 'white', height: 20, marginHorizontal: 30, marginTop: 6 }}>
                            <Text style={{ fontSize: 15 }}>Frequently Asked Questions</Text>
                        </View>
                        <Text button cardBody
                            onPress={
                                () => ToastAndroid.show('kosong', ToastAndroid.SHORT)
                            } style={{ height: 50, fontSize: 17, paddingTop: 12, paddingLeft: 40, color: '#7f8c8d' }}>What is Insurance ?</Text>
                        <View style={{ backgroundColor: '#bdc3c7', height: 2, marginHorizontal: 12 }}></View>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <View style={{ backgroundColor: 'white', height: 14, marginHorizontal: 30 }}>
                        </View>
                        <Text button cardBody
                            onPress={
                                () => ToastAndroid.show('kosong', ToastAndroid.SHORT)
                            } style={{ height: 50, fontSize: 15, marginBottom: 1, paddingLeft: 40, color: '#7f8c8d' }}>What should I do if my policy is lost or damaged ?</Text>
                        <View style={{ backgroundColor: '#bdc3c7', height: 2, marginHorizontal: 12 }}></View>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <View style={{ backgroundColor: 'white', height: 14, marginHorizontal: 30 }}>
                        </View>
                        <Text button cardBody
                            onPress={
                                () => ToastAndroid.show('kosong', ToastAndroid.SHORT)
                            } style={{ height: 50, fontSize: 15, marginBottom: 15, paddingLeft: 40, color: '#7f8c8d', paddingRight: 45 }}>What conditions must be met when I make changes to the policy ?</Text>
                        <View style={{ backgroundColor: '#bdc3c7', height: 2, marginHorizontal: 12 }}></View>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <View style={{ backgroundColor: 'white', height: 14, marginHorizontal: 30 }}>
                        </View>
                        <Text button cardBody
                            onPress={
                                () => ToastAndroid.show('kosong', ToastAndroid.SHORT)
                            } style={{ height: 50, fontSize: 15, marginBottom: 2, paddingLeft: 40, color: '#7f8c8d' }}>What should I do if my policy status is inactive ?</Text>
                        <View style={{ backgroundColor: '#bdc3c7', height: 2, marginHorizontal: 12 }}></View>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <View style={{ backgroundColor: 'white', height: 14, marginHorizontal: 30 }}>
                        </View>
                        <Text button cardBody
                            onPress={
                                () => ToastAndroid.show('kosong', ToastAndroid.SHORT)
                            } style={{ height: 50, fontSize: 15, marginBottom: 12, paddingLeft: 40, color: '#7f8c8d', paddingRight: 30 }}>If I have difficulty using MY AIA, where should I report it ?</Text>
                        <View style={{ backgroundColor: '#bdc3c7', height: 2, marginHorizontal: 12 }}></View>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <Text button cardBody
                            onPress={
                                () => ToastAndroid.show('kosong', ToastAndroid.SHORT)
                            } style={{ height: 50, fontSize: 17, paddingTop: 12, paddingLeft: 40, color: '#7f8c8d' }}>How can I get Asurane AGA Services?</Text>
                        <View style={{ backgroundColor: '#bdc3c7', height: 2, marginHorizontal: 12 }}></View>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <View style={{ backgroundColor: 'white', height: 14, marginHorizontal: 30 }}>
                        </View>
                        <Text button cardBody
                            onPress={
                                () => ToastAndroid.show('kososng', ToastAndroid.SHORT)
                            } style={{ height: 50, fontSize: 15, marginBottom: 12, paddingLeft: 40, color: '#7f8c8d', paddingRight: 30 }}>If I have not paid the Premium after passing the free period ?</Text>
                        <View style={{ backgroundColor: '#bdc3c7', height: 2, marginHorizontal: 12 }}></View>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <View style={{ backgroundColor: 'white', height: 14, marginHorizontal: 30 }}>
                        </View>
                        <Text button cardBody
                            onPress={
                                () => ToastAndroid.show('kososng', ToastAndroid.SHORT)
                            } style={{ height: 50, fontSize: 15, marginBottom: 12, paddingLeft: 40, color: '#7f8c8d', paddingRight: 30 }}>What are the conditions for paying premium via ATM ?</Text>
                        <View style={{ backgroundColor: '#bdc3c7', height: 2, marginHorizontal: 12 }}></View>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <View style={{ backgroundColor: 'white', height: 14, marginHorizontal: 30 }}>
                        </View>
                        <Text button cardBody
                            onPress={
                                () => ToastAndroid.show('kosong', ToastAndroid.SHORT)
                            } style={{ height: 50, fontSize: 15, marginBottom: 15, paddingLeft: 40, color: '#7f8c8d', paddingRight: 30 }}>What conditions must be met when I apply for Insurance Benefits?</Text>
                        <View style={{ backgroundColor: '#bdc3c7', height: 2, marginHorizontal: 12 }}></View>
                    </View>
                </ScrollView>
                </ImageBackground>
            </View>

        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default (connect(mapStateToProps)(Help));
