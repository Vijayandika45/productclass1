import React, { Component } from 'react'
import { Text, View, ImageBackground, TouchableOpacity, ToastAndroid } from 'react-native'
import { Card, Button, Item, Container, CardItem, Body, Input, Content } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction'


class login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: ''
        }
    }

    forgot = async () => {
        send = async (objParam) => await Axios.post(
            'https://asuransi-glints-academy.herokuapp.com/api/user/reset-password',
            objParam
        )

        console.log(this.state.email)

        send({
            email: this.state.email
        })
            .then(res => {
                
                this.props.navigation.navigate('Repassword')
                console.log('Berhasil!')
            })
            .catch(err => {
                this.setState({
                    Modal: false
                })
                ToastAndroid.show('Email Tidak ditemukan!', ToastAndroid.SHORT)
                console.log(this.state.email)
                console.log(err)
            })
    }

    render() {
        return (
            <KeyboardAwareScrollView>
                <Container>
                    <ImageBackground source={require('../img/ganteng2.png')} style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                        <Card transparent style={{ width: '80%', height: '65%', justifyContent: 'center', backgroundColor: 'rgba(255,255,255,0)' }}>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 20, color: '#f85f73', alignSelf: 'center', fontWeight: 'bold' }}>Forget Password</Text>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Body>
                                    <Item regular style={{
                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input placeholder="Email"
                                            onChangeText={(text) => this.setState({ email: text })}
                                            style={{ color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                <Button regular
                                    onPress={() => {
                                        this.forgot()
                                    }}
                                    style={{ backgroundColor: '#f85f73', width: '50%', justifyContent: 'center' }}>
                                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#fbe8d3' }}>Submit</Text>
                                </Button>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>

                                <Button transparent onPress={() => this.props.navigation.navigate('Forget')} style={{ justifyContent: 'center' }}>
                                    <Text style={{ alignSelf: 'center', color: '#f85f73', fontWeight: 'bold', fontSize: 18 }}>Forgot Password</Text>
                                </Button>

                            </CardItem>

                        </Card>
                    </ImageBackground>
                </Container>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: id => dispatch(auth(id))
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(login));