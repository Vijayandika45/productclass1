import React, { Component } from 'react'
import { Text, View, ImageBackground, TouchableOpacity, ToastAndroid } from 'react-native'
import { Card, Button, Item, Container, CardItem, Body, Input, Content } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { withNavigation } from 'react-navigation'
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction'


class login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            login: '',
            password: '',
            token: ''
        }
    }
    setData = async (nama, data) => {
        try {
            const storage = await AsyncSorage.setItem(`@${nama}`, data)//nyimpan data setItem
            console.log('Berhasil masukin data!')
        } catch (err) {
            console.log(err)
        }
    }

    getData = async (nama) => {
        try {
            const storage = await AsyncStorage.getItem(`@${nama}`)
            console.log('Berhasil Ambil Data !')
            return storage
        } catch (err) {
            console.log(err)
        }
    }

    login = async => {
        try {
            const masuk = async (objparam) => await Axios.post('https://asuransi-glints-academy.herokuapp.com/api/user/login', objparam
            )

            masuk({
                login: this.state.login,
                password: this.state.password
            })

                .then(res => {
                    console.log(res)
                    this.props.FetchId(res.data.result);//simpan data ke redux
                    console.log('token dari redux ', this.props.auth)             
                    if (res.data.role == 'Super_Admin') {
                        console.log('keluar adminnya gan')
                        this.props.navigation.navigate('Sahome')
                    }
                    else {
                        this.props.navigation.navigate('Uhome')
                    }

                    ToastAndroid.show('Welcome Gan', ToastAndroid.SHORT)

                })

                .catch(e => {
                    console.log(e)

                    ToastAndroid.show('Yah Gagal', ToastAndroid.SHORT)
                })

        } catch (err) {
            console.log(err)
        }
    }

    render() {
        return (
            <KeyboardAwareScrollView>
                <Container>
                    <ImageBackground source={require('../img/ganteng2.png')} style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                        <Card transparent style={{ width: '80%', height: '65%', justifyContent: 'center', backgroundColor: 'rgba(255,255,255,0)' }}>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 20, color: '#f85f73', alignSelf: 'center', fontWeight: 'bold' }}>Login</Text>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Body>
                                    <Item regular style={{
                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input placeholder="Username / Email"
                                            onChangeText={(text) => this.setState({ login: text })}
                                            style={{ color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)' }}>
                                <Body>
                                    <Item regular style={{
                                        borderColor: '#F62F5E',
                                        borderTopWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomWidth: 2
                                    }}>
                                        <Input secureTextEntry={true} placeholder="Password"
                                            onChangeText={(text) => this.setState({ password: text })}
                                            style={{ color: '#283c63', backgroundColor: 'rgba(255,255,255,0.6)' }} />
                                    </Item>
                                </Body>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                <Button regular
                                    onPress={() => {
                                        this.login()
                                    }}
                                    style={{ backgroundColor: '#f85f73', width: '50%', justifyContent: 'center' }}>
                                    <Text style={{ alignSelf: 'center', fontWeight: 'bold', color: '#fbe8d3' }}>Sign In</Text>
                                </Button>
                            </CardItem>

                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                <Body>
                                    <Text style={{ alignSelf: 'center', color: 'white', }}>Don't Have Account ?</Text>
                                    <Button transparent onPress={() => this.props.navigation.navigate('Register')} style={{ justifyContent: 'center' }}>
                                        <Text style={{ alignSelf: 'center', color: '#f85f73', fontWeight: 'bold', fontSize: 18 }}>Sign Up</Text>
                                    </Button>
                                </Body>
                            </CardItem>


                            <CardItem style={{ backgroundColor: 'rgba(0,0,0,0.2)', justifyContent: 'center' }}>
                                
                                    <Button transparent onPress={() => this.props.navigation.navigate('Forget')} style={{ justifyContent: 'center' }}>
                                        <Text style={{ alignSelf: 'center', color: '#f85f73', fontWeight: 'bold', fontSize: 18 }}>Forgot Password</Text>
                                    </Button>
                                
                            </CardItem>

                        </Card>
                    </ImageBackground>
                </Container>
            </KeyboardAwareScrollView>
        )
    }
}

const mapStateToProps = state => ({
    auth: state.auth
});

const mapDispatchToProps = (dispatch) => {
    return {
        FetchId: id => dispatch(auth(id))
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(login));