import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import LoginScreen from './src/pages/login'
import SahomeScreen from './src/pages/Sahome'
import RegisterScreen from './src/pages/Register'
import UhomeScreen from './src/pages/home'
import SplashScreen from './src/pages/Splash'
import DetailScreen from './src/pages/Details'
import ForgetScreen from './src/pages/ForgetScreen'
import RepasswordScreen from './src/pages/RepasswordScreen'

import { Provider } from 'react-redux';
import configureStore from './store';


const store = configureStore();

const Apps = createStackNavigator({
  Login: LoginScreen,
  Register: RegisterScreen,
  Sahome : SahomeScreen,
  Uhome : UhomeScreen,
  Splash : SplashScreen,
  Details : DetailScreen,
  Forget : ForgetScreen,
  Repassword : RepasswordScreen
},
  {
    initialRouteName: 'Login',
    headerMode: 'none'
  });

const AppContainer = createAppContainer(Apps)

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}
